#include "gpptrecid.h"

const guchar*
gppt_get_record_string (gint recid) {
  gint i;

  for(i=0;i<sizeof(recordstr)/sizeof(recordstr[0]);i++) {
    if( recordstr[i].id == recid )
      return recordstr[i].str;
  }

  return NULL;
}
