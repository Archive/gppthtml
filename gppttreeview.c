/*
   gppttreeview: ppt file tree struct viewer
   Copyright 2003 Yukihiro Nakai <ynakai@redhat.com>
   Copyright 2002 Charles N Wyble <jackshck@yahoo.com>  

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published  by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <gtk/gtk.h>

#include <gsf/gsf.h>
#include <gsf/gsf-input.h>
#include <gsf/gsf-infile.h>
#include <gsf/gsf-utils.h>
#include <gsf/gsf-input-stdio.h>
#include <gsf/gsf-infile-msole.h>

#include "gpptrecid.h"
#include "gpptrecord.h"

#define WORK_SIZE 8192

static gint buf_idx = 0;
static guchar working_buffer[WORK_SIZE];
static gint past_first_slide = 0;
static gint last_container = 0;

typedef struct _TreeItem TreeItem;

struct _TreeItem {
  const gchar* label;
  TreeItem *children;
};

enum {
  NAME_COLUMN = 0,
  NUM_COLUMNS
};

static TreeItem grandchild[] = {
  { "CHILD", NULL },
  { NULL }
};

static TreeItem rootentry[] = {
  { "TEST1", NULL },
  { NULL }
};

static GtkTreeModel* model = NULL;

static GList* layer_list = NULL;
static GList* rest_list = NULL; /* container data rest */

static void
finish_container() {
  GtkTreeIter *iter;

  if( layer_list == NULL )
    return; /* Nothing to do */

  iter = g_list_last(layer_list)->data;
  if( iter != NULL )
    g_free(iter);

  g_assert(g_list_length(layer_list) != 0);

  if( g_list_length(layer_list) == 1 ) {
    g_list_free(layer_list);
    g_list_free(rest_list);
    layer_list = NULL;
    rest_list = NULL;
  } else {
    /* Free the Container last layer */
    GList* last = g_list_last(layer_list);
    GList* prev = last->prev;
    prev->next = NULL;
    g_list_free_1(last);

    /* Free the Container length data */
    last = g_list_last(rest_list);
    prev = last->prev;
    prev->next = NULL;
    g_list_free_1(last);
  }
}

static void
add_new_item(GtkTreeModel* model, guchar* label, gulong length) {
  gint i;
  GtkTreeIter *iter;
  GtkTreeIter *parent_iter;
  gulong rest_data;
  
  g_return_if_fail(model != NULL);
  
  iter = g_new0(GtkTreeIter, 1);
  if( layer_list == NULL ) {
    parent_iter = NULL;
  } else {
    parent_iter = g_list_last(layer_list)->data;
  }
  
  gtk_tree_store_append(GTK_TREE_STORE(model), iter, parent_iter);
  gtk_tree_store_set (GTK_TREE_STORE(model), iter,
                        NAME_COLUMN, label,
                        -1);

  for(i=0;i<g_list_length(rest_list);i++) {
    rest_data = (gulong)(g_list_nth_data(rest_list, i));
    rest_data -= length + 8;
    g_list_nth(rest_list, i)->data = (gpointer)rest_data;
  }

  while(rest_list) {
    rest_data = (gulong)g_list_last(rest_list)->data;

    if( rest_data > 0 )
      break;
    if( rest_data == 0 )
      finish_container();
    else if( rest_data < 0 )
      g_print("Error: Atom length exceeds Container size.\n");
  }
}

static void
append_new_container(GtkTreeModel* model, guchar* label, gulong length) {
  GtkTreeIter *iter;
  GtkTreeIter *parent_iter;
  gulong rest_data;
  gint i;

  g_return_if_fail(model != NULL);

  iter = g_new0(GtkTreeIter, 1);
  if( layer_list == NULL ) {
    parent_iter = NULL;
  } else {
    parent_iter = g_list_last(layer_list)->data;
  }

  gtk_tree_store_append(GTK_TREE_STORE(model), iter, parent_iter);
  gtk_tree_store_set (GTK_TREE_STORE(model), iter,
                        NAME_COLUMN, label,
                        -1);

 /* Container uses 8 bytes as its header */
  for(i=0;i<g_list_length(rest_list);i++) {
    rest_data = (gulong)(g_list_nth_data(rest_list, i));
    rest_data -= 8;
    g_list_nth(rest_list, i)->data = (gpointer)rest_data;
  }

  layer_list = g_list_append(layer_list, iter);
  rest_list = g_list_append(rest_list, (gpointer)length);
}

static void
add_columns(GtkTreeView* treeview) {
  gint col_offset;
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;
  
  renderer = gtk_cell_renderer_text_new();
  g_object_set(G_OBJECT(renderer), "xalign", 0.0, NULL);
  
  col_offset = gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(treeview),  -1, "Name", renderer, "text", NAME_COLUMN, NULL);

  column = gtk_tree_view_get_column(GTK_TREE_VIEW(treeview), col_offset -1);
  gtk_tree_view_column_set_clickable(GTK_TREE_VIEW_COLUMN(column), TRUE);
}

static void
OutputCharCorrected(guchar c)
{
  switch (c) {	/* Special char handlers here... */
  case '\r':
    g_print("<BR>\n");
    break;
  case 0x3C:
    g_print("&lt;");
    break;
  case 0x3E:
    g_print("&gt;");
    break;
  case 0x26:
    g_print("&amp;");
    break;
  case 0x22:
    g_print("&quot;");
    break;
    /* Also need to cover 128-159 since MS uses this area... */
  case 0x80:		/* Euro Symbol */
    g_print("&#8364;");
    break;
  case 0x82:		/* baseline single quote */
    g_print("&#8218;");
    break;
  case 0x83:		/* florin */
    g_print("&#402;");
    break;
  case 0x84:		/* baseline double quote */
    g_print("&#8222;");
    break;
  case 0x85:		/* ellipsis */
    g_print("&#8230;");
    break;
  case 0x86:		/* dagger */
    g_print("&#8224;");
    break;
  case 0x87:		/* double dagger */
    g_print("&#8225;");
    break;
  case 0x88:		/* circumflex accent */
    g_print("&#710;");
    break;
  case 0x89:		/* permile */
    g_print("&#8240;");
    break;
  case 0x8A:		/* S Hacek */
    g_print("&#352;");
    break;
  case 0x8B:		/* left single guillemet */
    g_print("&#8249;");
    break;
  case 0x8C:		/* OE ligature */
    g_print("&#338;");
    break;
  case 0x8E:		/*  #LATIN CAPITAL LETTER Z WITH CARON */
    g_print("&#381;");
    break;
  case 0x91:		/* left single quote ? */
    g_print("&#8216;");
    break;
  case 0x92:		/* right single quote ? */
    g_print("&#8217;");
    break;
  case 0x93:		/* left double quote */
    g_print("&#8220;");
    break;
  case 0x94:		/* right double quote */
    g_print("&#8221;");
    break;
  case 0x95:		/* bullet */
    g_print("&#8226;");
    break;
  case 0x96:		/* endash */
    g_print("&#8211;");
    break;
  case 0x97:		/* emdash */
    g_print("&#8212;");
    break;
  case 0x98:		/* tilde accent */
    g_print("&#732;");
    break;
  case 0x99:		/* trademark ligature */
    g_print("&#8482;");
    break;
  case 0x9A:		/* s Haceks Hacek */
    g_print("&#353;");
    break;
  case 0x9B:		/* right single guillemet */
    g_print("&#8250;");
    break;
  case 0x9C:		/* oe ligature */
    g_print("&#339;");
    break;
  case 0x9F:		/* Y Dieresis */
    g_print("&#376;");
    break;
  default:
    putchar(c);
    break;
  }
}

static void
put_utf8(gushort c) {
  putchar(0x0080 | ((gshort)c & 0x003F));
}

static void
print_utf8(gushort c) {
  if( c == 0 )
    return;

  if(c < 0x80) {
    OutputCharCorrected(c);
  } else if( c < 0x800 ) {
    putchar( 0xC0 | (c >> 6 ) );
    put_utf8(c);
  } else {
    putchar(0xE0 | (c >> 12));
    put_utf8(c >>  6);
    put_utf8(c);
  }
}

static void
print_unicode(guchar *ucs, gint len) {
  gint i;
  for (i = 0; i < len; i += 2) {
    print_utf8(ucs[i] | (ucs[i+1] << 8));
  }
}

static void
container_processor(gint type) {
  if( type == GPPT_RECID_SLIDE ) {
    if( !past_first_slide )
      past_first_slide = 1;
  }

  last_container = type;
}

static void
atom_processor(gint type, gint count, gint buf_last, guchar data, gulong instance) {
  gint i;

  if( buf_idx >= WORK_SIZE )
    return;

  if( count == 0 ) {
    memset(working_buffer, 0, WORK_SIZE);
    buf_idx = 0;
  }

  if( count == buf_last ) {
    gchar* label = g_strdup_printf("[ATOM] type: %d(%s), count: %d, instance: %ld", type, gppt_get_record_string(type), count+1, instance);
    add_new_item(model, label, count+1);
    g_free(label);
  }

  switch(type) {
  case GPPT_RECID_TEXTCHARSATOM:
    working_buffer[buf_idx++] = data;
//    if( count == buf_last ) {
//      print_unicode(working_buffer, buf_idx);
//    }
    break;
  case GPPT_RECID_TEXTBYTESATOM:
    working_buffer[buf_idx++] = data;
//    if( count == buf_last) {
//      gint i;
//      for(i=0;i<buf_idx;i++) {
//        if( working_buffer[i] == 0x0D )
//          g_print("<BR>\n");
//        else
//          putchar(working_buffer[i]);
//      }
//      g_print("<BR>\n");
//    }
    break;
  case GPPT_RECID_CSTRING:
    working_buffer[buf_idx++] = data;
//    if( count == buf_last ) {
//      print_unicode(working_buffer, buf_idx);
//      g_print("<BR>\n");
//    }
    break;
  case GPPT_RECID_FONTENTITYATOM:
    working_buffer[buf_idx++] = data;
//    if( count == buf_last ) {
//      for(i=0;i<buf_idx;i += 2) {
//        if( working_buffer[i] == 0 && working_buffer[i+1] == 0 ) {
//          print_unicode(working_buffer, i);
//          g_print("<BR>\n");
//          break;
//        }
//      }
//    }
    break;
  case GPPT_RECID_OEPLACEHOLDERATOM:
    working_buffer[buf_idx++] = data;
//    if( count == buf_last ) {
//      g_print("placementID: 0x%lx<BR>\n", GSF_LE_GET_GUINT32(working_buffer));
//      g_print("PlaceholderId: %u<BR>\n", working_buffer[4]);
//      g_print("size: %d<BR>\n", working_buffer[5]);
//    }
    break;
  case GPPT_RECID_SLIDEATOM:
    working_buffer[buf_idx++] = data;
//    if( count == buf_last ) {
//      g_print("masterID: 0x%lx<BR>\n", GSF_LE_GET_GINT32(working_buffer+12));
//      g_print("notesID: 0x%lx<BR>\n", GSF_LE_GET_GINT32(working_buffer+16));
//      g_print("Flags: 0x%x<BR>\n", working_buffer[20]);
//      g_print("Flags: 0x%x<BR>\n", working_buffer[21]);
//      g_print("?: 0x%x<BR>\n", working_buffer[22]);
//      g_print("?: 0x%x<BR>\n", working_buffer[23]);
//    }
    break;
  case GPPT_RECID_SLIDEPERSISTATOM: /* S6303C.HTM */
    working_buffer[buf_idx++] = data;
//    if( count == buf_last ) {
//      g_print("psrReference: 0x%lx<BR>\n", GSF_LE_GET_GUINT32(working_buffer));
//      g_print("flags: 0x%lx<BR>\n", GSF_LE_GET_GUINT32(working_buffer+4));
//      g_print("numberTexts: 0x%lx<BR>\n", GSF_LE_GET_GINT32(working_buffer+8));
//      g_print("slideId: 0x%lx<BR>\n", GSF_LE_GET_GINT32(working_buffer+12));
//      g_print("Reserved: 0x%lx<BR>\n", GSF_LE_GET_GUINT32(working_buffer+16));
//    }
    break;
  case GPPT_RECID_TEXTHEADERATOM: /* S6304E.HTM */
    working_buffer[buf_idx++] = data;
 //   if( count == buf_last ) {
 //     g_print("text type: %d<BR>\n", GSF_LE_GET_GUINT32(working_buffer));
 //   }
    break;
  case GPPT_RECID_SLIDEVIEWINFOATOM: /* S6303E.HTM */
    working_buffer[buf_idx++] = data;
//    if( count == buf_last ) {
//      g_print("showGuides: %d<BR>\n", working_buffer[0]);
//      g_print("snapToGrid: %d<BR>\n", working_buffer[0]);
//      g_print("snapToShape: %d<BR>\n", working_buffer[0]);
//    }
    break;
  case GPPT_RECID_HEADERSFOOTERSATOM: /* S6301D.HTM */
    working_buffer[buf_idx++] = data;
//    if( count == buf_last ) {
//      g_print("FormatId: %d<BR>\n", GSF_LE_GET_GINT16(working_buffer));
//      g_print("Flags: 0x%x<BR>\n", GSF_LE_GET_GUINT16(working_buffer));
//    }
    break;
  case GPPT_RECID_OUTLINETEXTREFATOM: /* S6302A.HTM */
    working_buffer[buf_idx++] = data;
//    if( count == buf_last ) {
//      g_print("Index: %d<BR>\n", GSF_LE_GET_GINT32(working_buffer));
//    }
    break;
  default:
    working_buffer[buf_idx++] = data;
    break;
  }
}

static void
ppt_read(GsfInput *input) {
  guint8 const *data;
  gulong version=0, instance=0, type = 0, length=0, target=0, count=0;

  while(!gsf_input_eof(input)) {
    data = gsf_input_read(input, 1, NULL);
    
    if( count == 0 ) {
      instance = data[0];
      type = 0;
      length = 0;
      target = 80; /* fictitious num */
    } else if ( count == 1 ) {
      instance |= (data[0]<<8);
      version = instance & 0x000F;
      instance = (instance>>4);
    } else if ( count == 2 ) {
      type = data[0];
    } else if ( count == 3 ) {
      type |= (data[0]<<8)&0x00000FFFL;
    } else if ( count == 4 ) {
      length = data[0];
    } else if ( count == 5 ) {
      length |= (data[0]<<8);
    } else if ( count == 6 ) {
      length |= (data[0]<<16);
    } else if ( count == 7 ) {
      length |= (data[0]<<24);
      target = length;
      if( version == 0x0F ) {
        gchar* label = g_strdup_printf("[CONTAINER] type: %d(%s), length: %ld", type, gppt_get_record_string(type), length);
        container_processor(type);
        append_new_container(model, label, length);
        g_free(label);

        count = -1;
      }
    } else if ( count > 7 ) {
        atom_processor(type, count-8, target-1, data[0], instance);
    }

    if( count == (target+7))
      count = 0;
    else
      count++;
  }

}

int main(int argc, char** argv) {
  GsfInput *input;
  GsfInput *pptinput;
  GsfInfile *infile;
  GError *err;

  GtkWidget* window;
  GtkWidget* sw;
  GtkWidget* treeview;

  if( argc < 2 ) {
    g_print("Usage: gppthtml filename\n");
    exit(0);
  }

  gtk_set_locale();
  gtk_init(&argc, &argv);
  gsf_init();

  input = GSF_INPUT(gsf_input_stdio_new(argv[1], &err));
  if( input == NULL ) {
    g_warning("file error: %s\n", argv[1]);
    gsf_shutdown();
    exit(1);
  }

  input = gsf_input_uncompress(input);
  infile = GSF_INFILE (gsf_infile_msole_new(input, &err));
  g_object_unref(G_OBJECT(input));

  if( infile == NULL ) {
    g_warning("Not an OLE file: %s\n", argv[1]);
    gsf_shutdown();
    exit(1);
  }

  pptinput = gsf_infile_child_by_name(infile, "PowerPoint Document");

  if( pptinput == NULL ) {
    g_warning("Not PowerPoint file\n");
    gsf_shutdown();
    exit(1);
  }

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(window), "PPT File Tree Viewer");
  g_signal_connect(window, "destroy",
                   G_CALLBACK(gtk_main_quit), &window);
  sw = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
                                 GTK_POLICY_AUTOMATIC,
                                 GTK_POLICY_AUTOMATIC);
  model = GTK_TREE_MODEL(gtk_tree_store_new(NUM_COLUMNS, G_TYPE_STRING));

  ppt_read(pptinput);

  treeview = gtk_tree_view_new_with_model(model);
  g_object_unref(model);

  gtk_tree_view_set_rules_hint(GTK_TREE_VIEW(treeview), TRUE);
  gtk_container_add(GTK_CONTAINER(sw), treeview);
  g_signal_connect(treeview, "realize",
                   G_CALLBACK(gtk_tree_view_expand_all), NULL);
  add_columns(GTK_TREE_VIEW(treeview));
  gtk_container_add(GTK_CONTAINER(window), sw);
  gtk_window_set_default_size(GTK_WINDOW(window), 600, 400);
  gtk_widget_show_all(window);

  gtk_main();

  gsf_shutdown();

  return 0;
}
