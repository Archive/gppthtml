#ifndef __G_UNICODE_JP_H__
#define __G_UNICODE_JP_H__

#include <glib/gerror.h>
#include <glib/gtypes.h>
#include <glib/gunicode.h>

gboolean g_unichar_ishiragana   (gunichar c) G_GNUC_CONST;
gboolean g_unichar_iskatakana   (gunichar c) G_GNUC_CONST;
gboolean g_unichar_iskanji	(gunichar c) G_GNUC_CONST;
gboolean g_unichar_isjapanese	(gunichar c) G_GNUC_CONST;


#endif /* __G_UNICODE_JP_H__ */

