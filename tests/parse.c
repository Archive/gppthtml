/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *
 * Copyright (C) 2005 Yukihiro Nakai <nakai@gnome.gr.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2.1 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <gsf/gsf.h>
#include <gsf/gsf-input.h>
#include <gsf/gsf-output.h>
#include <gsf/gsf-infile.h>
#include <gsf/gsf-outfile.h>
#include <gsf/gsf-utils.h>
#include <gsf/gsf-input-stdio.h>
#include <gsf/gsf-output-stdio.h>
#include <gsf/gsf-infile-msole.h>
#include <gsf/gsf-outfile-msole.h>
#include <gsf/gsf-msole-utils.h>

#include <glib/gprintf.h>

#include "gpptrecid.h"
#include "gpptrecord.h"
#include "dumputf8.h"
#include "parse.h"

#include "escape.h"

GSList*
parse_container(GsfInput* input, MSContainer *cur_container)
{
  GSList* root_slist = NULL;
  guint8 const *data;
  MSObj* cur_obj = MS_OBJ(cur_container);

  /*
  if( cur_container ) {
    gsf_input_seek(input, cur_obj->index, G_SEEK_SET);
  } else {
    gsf_input_seek(input, 0, G_SEEK_SET);
  }
  */
  
  if( cur_container && gsf_input_remaining(input) < cur_obj->length ) {
    g_error("parse_container() exceeded.\n");
  }

  while(gsf_input_remaining(input) > 7) {
    gulong version=0, instance=0, id=0, length=0;
    data = gsf_input_read(input, 8, NULL);
    instance = GSF_LE_GET_GUINT16(data);
    version = instance & 0x000F;
    instance >>= 4;
//    id = GSF_LE_GET_GUINT16 (data+2) & 0x00000FFFL; /* FIXME: ppt has some data in 0xF000 part. I cannot tell that's id or not. */
    id = GSF_LE_GET_GUINT16 (data+2);
    length = GSF_LE_GET_GUINT32 (data+4);

    if( version == 0x0F ) {
      MSObj* obj = ms_container_new();
      MSContainer* container = MS_CONTAINER(obj);
      obj->instance = instance;
      obj->version = 0x0F;
      obj->id = id;
      obj->index = gsf_input_tell(input);
      obj->length = length;
      container->parent = cur_container;
      if( !cur_container ) {
        root_slist = g_slist_append(root_slist, obj);
      } else {
        cur_container->obj_slist = g_slist_append(cur_container->obj_slist, obj);
      }
      //g_print("[CONTAINER] id: %d, length: %ld, %p\n", id, length, cur_container);
      //gsf_input_seek(input, length, G_SEEK_CUR);
      parse_container(input, container); /* Recurse */
    } else { /* Atom */
      MSObj* obj = ms_atom_new();
      MSAtom* atom = MS_ATOM(obj);
      obj->instance = instance;
      obj->version = version;
      obj->id = id;
      obj->index = gsf_input_tell(input);
      obj->length = length;
      {
      gsf_input_seek(input, obj->index, G_SEEK_SET);
      guint8 const *mydata = gsf_input_read(input, obj->length, NULL);
      //obj->data = g_memdup(mydata, obj->length);
      atom->data = g_new0(guint8, obj->length);
      memcpy(atom->data, mydata, obj->length);
      gsf_input_seek(input, obj->index, G_SEEK_SET);
      }
      if ( !cur_container ) {
        root_slist = g_slist_append(root_slist, obj);
      } else {
        cur_container->obj_slist = g_slist_append(cur_container->obj_slist, obj);
      }
      //g_print("[ATOM] id: %d, length: %ld, %p\n", id, length, cur_container);
      gsf_input_seek(input, length, G_SEEK_CUR);
    }

    if( cur_container && gsf_input_tell(input) >= cur_obj->index + cur_obj->length ) {
      /* index exceeded - Error */
      return root_slist;
    }
  }

  return root_slist;
}

GSList*
parse_file (gchar* filename)
{
  GError *err;
  GsfInput *input;
  GsfInfile *infile;
  GsfInput *content;
  GSList* root_slist = NULL;

  g_return_val_if_fail(filename != NULL, NULL);
  g_return_val_if_fail(*filename != '\0', NULL);

  input = GSF_INPUT(gsf_input_stdio_new(filename, &err));
  if (input == NULL) {
    g_print("gsf_input_stdio_new() failed.\n");
    gsf_shutdown();
    exit(1);
  }

  infile = GSF_INFILE (gsf_infile_msole_new(input, &err));
  content = gsf_infile_child_by_name(infile, "PowerPoint Document");
  root_slist = parse_container(content, NULL);

  return root_slist;
}

//gint g_eseq_printf (gchar const *format, ...) G_GNUC_PRINTF (1, 2);
gint g_eseq_printf (gchar const *format, ...)
{
  va_list args;
  gint ret;

  g_print("%s", eseq_cap);
  va_start (args, format);
  ret = g_vprintf(format, args);
  g_print("%s", eseq_end);
  va_end (args);

  return ret;
}

typedef gint (*GPPTPrintFunc) (const gchar *format, ...);

void
msobj_compare (MSObj* obj_a, MSObj* obj_b, gint depth)
{
  GPPTPrintFunc printfunc;
  gint i=0;

  if (!obj_a || obj_a->id != obj_b->id) {
    g_print("%s", eseq_cap);
    ms_obj_dump(obj_b, depth);
    g_print("%s", eseq_end);
    return;
  }

  gchar* spcstr = g_new0(gchar, depth*2+1);
  memset(spcstr, ' ', depth*2);
  g_print("%s", spcstr);

  g_print(MS_IS_CONTAINER(obj_b) ? "[CONTAINER] " : "[ATOM] ");
  g_print("id=%ld(%s), ", obj_b->id, gppt_get_record_string(obj_b->id));
  printfunc = obj_a->index != obj_b->index ? g_eseq_printf : g_printf;
  printfunc("length=%ld", obj_b->length);
  g_print(", ");
  printfunc = obj_a->instance != obj_b->instance ? g_eseq_printf : g_printf;
  printfunc("instance=%ld", obj_b->instance);
  g_print(", ");
  printfunc = obj_a->version != obj_b->version ? g_eseq_printf : g_printf;
  printfunc("version=%ld", obj_b->version);
  g_print("\n");

  if( MS_IS_CONTAINER(obj_b) ) {
    MSContainer* container_a = MS_CONTAINER(obj_a);
    MSContainer* container_b = MS_CONTAINER(obj_b);
    gint objlen_a = g_slist_length(container_a->obj_slist);
    gint objlen_b = g_slist_length(container_b->obj_slist);
    if( objlen_a != objlen_b ) { /* Give up */
      g_warning("container obj length is not same");
    }
    for(i=0;i<objlen_b;i++) {
      MSObj* myobj_a = g_slist_nth_data(container_a->obj_slist, i);
      MSObj* myobj_b = g_slist_nth_data(container_b->obj_slist, i);
      msobj_compare(myobj_a, myobj_b, depth+1);
    }
  } else if( MS_IS_ATOM(obj_b) ) {
    MSAtom* atom_a = MS_ATOM(obj_a);
    MSAtom* atom_b = MS_ATOM(obj_b);
    gppt_mem_compare_dump(atom_a->data, atom_b->data, obj_a->length, obj_b->length);
  }
}

/*
 * Compare container/atom tree and dump
 */
void
compare_tree (GSList* tree_a, GSList* tree_b)
{
  gint len_a = g_slist_length(tree_a);
  gint len_b = g_slist_length(tree_b);
  gint i;

  for(i=0;i<len_b;i++) {
    if(i >= len_a) {
      g_warning("B is bigger than A\n");
      return;
    }
    MSObj* obj_a = g_slist_nth_data(tree_a, i);
    MSObj* obj_b = g_slist_nth_data(tree_b, i);
    msobj_compare(obj_a, obj_b, 0);
  }
}
