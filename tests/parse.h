/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *
 * Copyright (C) 2005 Yukihiro Nakai <nakai@gnome.gr.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2.1 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

#ifndef _PARSE_H
#define _PARSE_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <gsf/gsf.h>
#include <gsf/gsf-input.h>

#include "msobj.h"

void compare_tree(GSList* tree_a, GSList* tree_b);
GSList* parse_container(GsfInput* input, MSContainer *cur_container);
GSList* parse_file(gchar* filename);

#endif /* _PARSE_H */
