/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *
 * Copyright (C) 2005 Yukihiro Nakai <nakai@gnome.gr.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2.1 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <gsf/gsf-utils.h>

#include "gpptrecid.h"
#include "gpptrecord.h"
#include "dumputf8.h"

#include "msobj.h"

void
ms_obj_recalc_index (MSObj* obj)
{
  gulong index = obj->index;

  if (MS_IS_CONTAINER(obj)) {
    MSContainer* container = MS_CONTAINER(obj);
    GSList* slist = container->obj_slist;
    while(slist) {
      MSObj* myobj = slist->data;
      index += 8;
      myobj->index = index;
      index += myobj->length;
      ms_obj_recalc_index(myobj);
      slist = slist->next;
    }
  }
}

void
ms_obj_recalc_length(MSObj* obj)
{
  if(obj->recalc_length) { /* MSAtom doesn't have this function */
    obj->recalc_length(obj);
  }
}

void
ms_container_recalc_length(MSObj* obj)
{
  MSContainer* container = MS_CONTAINER(obj);
  gulong total_length = 0;
  GSList* slist = container->obj_slist;
  while(slist) {
    MSObj* myobj = slist->data;

#if 0
    if (myobj->id == GPPT_RECID_TEXTCHARSATOM) {
      MSAtom* atom = MS_ATOM(myobj);
      atom->data = g_new0(guint8, 5);
      atom->data[0] = 0x42;
      atom->data[1] = 0x30;
      atom->data[2] = 0x42;
      atom->data[3] = 0x30;
      obj->length = 4;
    }
#endif

    if (myobj->recalc_length) {
      ms_obj_recalc_length(myobj);
    }
    total_length += myobj->length + 8;
    slist = slist->next;
  }

  obj->length = total_length;
}

void
ms_obj_dump(MSObj* obj, gint depth)
{
  obj->dump(obj, depth);
}

/* Print spaces of number of depth*2 */
static void
gppt_print_depth(gint depth)
{
  gchar* spc = g_new0(gchar, depth*2+1);
  memset(spc, 0x20, depth*2);
  g_print("%s", spc);
  g_free(spc);
}

void
ms_obj_write_ppt_header(MSObj* obj, guint8* data)
{
    guint8* cursor = data + obj->index - 8;
    gulong instance, version, id, length;

    instance = obj->instance;
    version = obj->version;
    id = obj->id;
    length = obj->length;

    GSF_LE_SET_GUINT16(cursor, (instance<<4) | (version & 0x0F) );
    GSF_LE_SET_GUINT16(cursor+2, id);
    GSF_LE_SET_GUINT32(cursor+4, length);
}

void
ms_obj_write_ppt (MSObj* obj, guint8* data)
{
  obj->write_ppt(obj, data);
}

void
ms_container_write_ppt(MSObj* obj, guint8* data)
{
  MSContainer* container = MS_CONTAINER(obj);
  GSList* slist;

  g_return_if_fail(data != NULL);

  ms_obj_write_ppt_header(obj, data);

  if (!container->obj_slist)
    return;

  slist = container->obj_slist;
  while (slist) {
    MSObj* myobj = slist->data;
    myobj->write_ppt(myobj, data);
    slist = slist->next;
  }
}

void
ms_atom_write_ppt(MSObj* obj, guint8* data)
{
  MSAtom* atom = MS_ATOM(obj);
  guint8* cursor = data + obj->index;

  g_return_if_fail(data != NULL);

  ms_obj_write_ppt_header(obj, data);
  memcpy(cursor, atom->data, obj->length);
}

void
ms_container_dump(MSObj* obj, gint depth) {
  MSContainer* container = MS_CONTAINER(obj);

  gppt_print_depth(depth);

  g_print("[CONTAINER] id=%ld(%s), length=%ld, instance=%ld\n", obj->id, gppt_get_record_string(obj->id), obj->length, obj->instance);
  if( container->obj_slist ) {
    GSList* cur = container->obj_slist;
    while(cur) {
      MSObj* myobj = (MSObj*)(cur->data);
      ms_obj_dump(myobj, depth+1);
      cur = cur->next;
    }
  }
}

void
ms_atom_dump(MSObj* obj, gint depth)
{
  MSAtom* atom = MS_ATOM(obj);

  gppt_print_depth(depth);

  g_print("[ATOM] id=%ld(%s), length=%ld, instance=%ld\n", obj->id, gppt_get_record_string(obj->id), obj->length, obj->instance);

  switch (obj->id) {
  case GPPT_RECID_TEXTCHARSATOM:
  case GPPT_RECID_FONTENTITYATOM:
    gppt_mem_dump_utf8(atom->data, obj->length); 
    break;
  default:
    gppt_mem_dump(atom->data, obj->length); 
    break; 
  }
}

MSObj*
ms_container_new()
{
  MSContainer* result = g_new0(MSContainer, 1);
  MSObj* obj = MS_OBJ (result);

  obj->type = PPT_TYPE_CONTAINER;
  obj->dump = ms_container_dump;
  obj->write_ppt = ms_container_write_ppt;
  obj->recalc_length = ms_container_recalc_length;

  return obj;
}

MSObj*
ms_atom_new()
{
  MSAtom* result = g_new0(MSAtom, 1);
  MSObj* obj = MS_OBJ (result);

  obj->type = PPT_TYPE_ATOM;
  obj->dump = ms_atom_dump;
  obj->write_ppt = ms_atom_write_ppt;
  obj->recalc_length = NULL;

  return obj;
}
