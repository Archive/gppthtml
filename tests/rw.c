/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *
 * Copyright (C) 2005 Yukihiro Nakai <nakai@gnome.gr.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2.1 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <gsf/gsf.h>
#include <gsf/gsf-input.h>
#include <gsf/gsf-output.h>
#include <gsf/gsf-infile.h>
#include <gsf/gsf-outfile.h>
#include <gsf/gsf-utils.h>
#include <gsf/gsf-input-stdio.h>
#include <gsf/gsf-output-stdio.h>
#include <gsf/gsf-infile-msole.h>
#include <gsf/gsf-outfile-msole.h>
#include <gsf/gsf-msole-utils.h>
#include <locale.h>

#include "gpptrecid.h"
#include "gpptrecord.h"
#include "dumputf8.h"
#include "msobj.h"
#include "parse.h"

//static GSList* root_slist = NULL; /* Object list */

void*
ghfunc(gpointer key, gpointer value, GsfDocMetaData *metadata) {
  GsfDocProp* prop = gsf_doc_meta_data_get_prop(metadata, key);
  g_print("key: %s\n", (gchar*)key);
  g_print("value: %s\n", g_strdup_value_contents(prop->val));

  return NULL;
}

void
dump_metadata(GsfInfile* infile, const gchar* name) {
  GError *err;
  GsfInput* pptinput = gsf_infile_child_by_name(infile, name);
  GsfDocMetaData* metadata = gsf_msole_metadata_read_real(pptinput, &err);
  if (metadata)
    gsf_doc_meta_data_foreach(metadata, (GHFunc)ghfunc, metadata);
}

/*
 * Recalculate sizes and indices
 */
void
recalc_lengths(GSList* root_slist)
{
  gulong index = 0;
  GSList* slist = root_slist;

  /* First, make lengths correctly. */
  while(slist) {
    MSObj* obj = slist->data;
    ms_obj_recalc_length(obj);
    slist = slist->next;
  }
  /* Next, make indices correctly. */
  while(slist) {
    MSObj* obj = slist->data;
    index += 8;
    obj->index = index;
    ms_obj_recalc_index(obj);
    index += obj->length;
    slist = slist->next;
  }
}

guint8*
generate_ppt(GSList* root_slist)
{
	guint8 *data;
	GSList* slist = root_slist;
	gulong total_size = 0;
	while(slist) {
		MSObj* obj = MS_OBJ(slist->data);
		g_assert(obj != NULL);
		total_size += 8 + obj->length;
		slist = slist->next;
	}
	data = g_new0(guint8, total_size);
	slist = root_slist; /* Loop again */
	while(slist) {
		MSObj* obj = MS_OBJ(slist->data);
		ms_obj_write_ppt(obj, data);
		slist = slist->next;
	}

	return data;
}

int main(int argc, char** argv) {
  GSList* root_slist = NULL;
  GsfInput *input = NULL;
  GsfInput *pptinput = NULL;
  GsfOutput *output;
  GsfInfile *infile;
  GsfOutfile *outfile;
  GError *err = NULL;
  gsf_off_t size;
  guint8 const *data;

  if (argc < 2) {
    g_print("Usage: rw filename\n");
    exit(1);
  }

  setlocale(LC_ALL, "");
  gsf_init();

  input = GSF_INPUT(gsf_input_stdio_new(argv[1], &err));
  if( input == NULL || err ) {
    g_print("gsf_input_stdio_new() failed: ");
    if (err && err->message) {
      g_print("%s", err->message);
    }
    g_print("\n");
    gsf_shutdown();
    exit(1);
  }

  g_assert(GSF_IS_INPUT(input));
  input = gsf_input_uncompress(input);
  infile = GSF_INFILE (gsf_infile_msole_new(input, &err));
  if( infile == NULL || err ) {
    g_print("gsf_infile_msole_new() failed: ");
    if (err && err->message) {
      g_print("%s", err->message);
    }
    g_print("\n");
    gsf_shutdown();
    exit(1);
  }

#if 0
  {
    /* Dump child names */
    int i; int num;
    num = gsf_infile_num_children(infile);
    for(i=0;i<num;i++) {
      pptinput = gsf_infile_child_by_index(infile, i);
      g_print("name: %s\n", gsf_input_name(pptinput));
      g_print("%x\n", pptinput);
    }
  }
#endif

  /* Dump metadata */
  /*
  dump_metadata(infile, "\05DocumentSummaryInformation");
  dump_metadata(infile, "\05SummaryInformation");
  */

  guint8 const *docdata;

  pptinput = gsf_infile_child_by_name(infile, "PowerPoint Document");
  size = gsf_input_size(pptinput);
  docdata = gsf_input_read(pptinput, size, NULL);
  gsf_input_seek(pptinput, 0, G_SEEK_SET);

  //root_slist = parse_container(pptinput, NULL);
  root_slist = parse_file(argv[1]);

#if 0
  { /* Dump containers/atoms */
    int i;
    for(i=0;i<g_slist_length(root_slist);i++) {
      MSObj* obj = g_slist_nth_data(root_slist, i);
      ms_obj_dump(obj, 0);
    }
  }
#endif

  gsf_input_seek(input, 0, G_SEEK_SET);
  size = gsf_input_size(input);
  data = gsf_input_read(input, size, NULL);

  output = GSF_OUTPUT(gsf_output_stdio_new("output.ppt", &err));
  if (output == NULL) {
    g_print("gsf_output_stdio_new()\n");
    gsf_shutdown();
    exit(1);
  }

  input = GSF_INPUT(gsf_input_stdio_new(argv[1], &err));
  infile = GSF_INFILE (gsf_infile_msole_new(input, &err));
  outfile = gsf_outfile_msole_new(output);
  GsfOutput* content;

  pptinput = gsf_infile_child_by_name(infile, "PowerPoint Document");
  content = gsf_outfile_new_child(outfile, gsf_input_name(pptinput), FALSE);
  size = gsf_input_size(pptinput);
  data = gsf_input_read(pptinput, size, NULL);
  //gsf_output_write(content, size, data);
  recalc_lengths(root_slist);
  gsf_output_write(content, size, generate_ppt(root_slist));
  gsf_output_close(content);

  gsf_output_close(GSF_OUTPUT(outfile));

  /* libgsf dropping hole - do not do like below */
//  gsf_output_write(output, size, data);
//  gsf_output_close(output);

  g_print("\n");

  gsf_shutdown();
  return 0;
}
