/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *
 * Copyright (C) 2005 Yukihiro Nakai <nakai@gnome.gr.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2.1 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

#ifndef __MS_OBJ_H
#define __MS_OBJ_H

#include <glib.h>

typedef enum {
  PPT_TYPE_CONTAINER,
  PPT_TYPE_ATOM,
} PptType;

typedef struct _MSContainer MSContainer;
typedef struct _MSAtom MSAtom;
typedef struct _MSObj MSObj;

struct _MSObj {
  PptType type;
  gulong id;
  gulong index;
  gulong length;
  gulong instance;
  gulong version;

  void (* dump) (MSObj* obj, gint depth); /* Dump ppt binaries */
  void (* write_ppt) (MSObj* obj, guint8* data); /* Write ppt format to data */
  void (* recalc_length) (MSObj* obj); /* Recalculate length */
  void (* recalc_index) (MSObj* obj); /* Recalculate index */
};

struct _MSContainer {
  MSObj obj;
  GSList* obj_slist; /* GSList of children */
  guint8 *data;
  MSContainer *parent; /* Parent MSContainer */
};

struct _MSAtom {
  MSObj obj;
  guint8 *data;
  MSContainer *parent; /* Parent MSContainer */
};

#define MS_CONTAINER(obj) ((MSContainer*)obj)
#define MS_ATOM(obj) ((MSAtom*)obj)
#define MS_OBJ(obj) ((MSObj*)obj)
#define MS_IS_CONTAINER(obj) (obj->type == PPT_TYPE_CONTAINER)
#define MS_IS_ATOM(obj) (obj->type == PPT_TYPE_ATOM)

void ms_obj_recalc_length (MSObj* obj);
void ms_obj_recalc_index (MSObj* obj);
void ms_obj_dump(MSObj* obj, gint depth);
void ms_obj_write_ppt_header(MSObj* obj, guint8* data);
void ms_obj_write_ppt (MSObj* obj, guint8* data);
void ms_container_write_ppt(MSObj* obj, guint8* data);
void ms_atom_write_ppt(MSObj* obj, guint8* data);
void ms_container_dump(MSObj* obj, gint depth);
void ms_atom_dump(MSObj* obj, gint depth);
MSObj* ms_container_new();
MSObj* ms_atom_new();

#endif /* __MSOBJ_H */
