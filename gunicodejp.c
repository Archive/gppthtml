#include "gunicodejp.h"

/*
 * Hiragana mapping:
 *   0x3041-0x3096
 *   0x309D-0x309F
 *
 */
gboolean
g_unichar_ishiragana (gunichar c)
{
	if (c >= 0x3041 && c <= 0x3096)
		return TRUE;

	return (c >= 0x309D && c <= 0x309F) ? TRUE : FALSE;
}

/*
 * Katakana mapping:
 *   0x30A1-0x30FA
 *   0x30FD-0x30FE
 *   0x30FF
 *   0x31F0-0x31FF
 *   0xFF66-0xFF6F
 *   0xFF71-0xFF9D
 */
gboolean
g_unichar_iskatakana (gunichar c)
{
	if (c >= 0x30A1 && c <= 0x30FA)
		return TRUE;

	if (c >= 0x30FD && c <= 0x30FF)
		return TRUE;

	if (c >= 0x31F0 && c <= 0x31FF)
		return TRUE;

	if (c >= 0xFF66 && c <= 0xFF6F)
		return TRUE;

	return (c >= 0xFF71 && c <= 0xFF9D) ? TRUE : FALSE;
}

/*
 * Han character mapping:
 *   0x2E80-0x2E99
 *   0x2E9B-0x2EF3
 *   0x2F00-0x2FD5
 *   0x3005
 *   0x3007
 *   0x3021-0x3029
 *   0x3038-0x303A
 *   0x303B
 *   0x3400-0x4DB5
 *   0x4E00-0x9FBB
 *   0xF900-0xFA2D
 *   0xFA30-0xFA6A
 *   0xFA70-0xFAD9
 *   0x20000-0x2A6D6
 *   0x2F800-0x2FA1D
 */
gboolean
g_unichar_iskanji (gunichar c)
{
	if (c >= 0x2E80 && c <= 0x2E99)
		return TRUE;

	if (c >= 0x2E9B && c <= 0x2EF3)
		return TRUE;

	if (c >= 0x2F00 && c <= 0x2FD5)
		return TRUE;

	if (c == 0x3005 || c == 0x3007)
		return TRUE;

	if (c >= 0x3021 && c <= 0x3029)
		return TRUE;

	if (c >= 0x3038 && c <= 0x303B)
		return TRUE;

	if (c >= 0x3400 && c <= 0x4DB5)
		return TRUE;

	if (c >= 0x4E00 && c <= 0x9FBB)
		return TRUE;

	if (c >= 0xF900 && c <= 0xFA2D)
		return TRUE;

	if (c >= 0xFA30 && c <= 0xFA6A)
		return TRUE;

	if (c >= 0xFA70 && c <= 0xFAD9)
		return TRUE;

	return FALSE;
}

gboolean
g_unichar_isjapanese (gunichar c)
{
	return (g_unichar_ishiragana(c) || g_unichar_iskatakana(c) || g_unichar_iskanji(c)) ? TRUE : FALSE;
}
