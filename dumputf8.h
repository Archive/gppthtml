/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *
 * Copyright (C) 2005 Yukihiro Nakai <nakai@gnome.gr.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2.1 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

#ifndef _DUMP_UTF8_H
#define _DUMP_UTF8_H

#include <glib.h>

void gppt_mem_dump_utf8 (guint8 const *ptr, size_t len);
void gppt_mem_dump (guint8 const *ptr, size_t len);
void gppt_mem_compare_dump (guint8 const *ptr_a, guint8 const *ptr_b, size_t len_a, size_t len_b);

#endif /* _DUMP_UTF8_H */
