/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * dumputf8.c: 
 *
 * Copyright (C) 2005 Yukihiro Nakai <nakai@gnome.gr.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2.1 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

#include <stdio.h>
#include <glib.h>

#include "gunicodejp.h"
#include "dumputf8.h"
#include "escape.h"

void
gppt_mem_dump_utf8 (guint8 const *ptr, size_t len)
{
 int i = 0;
 int j = 0;
 gchar writebuf[100];
 gchar workbuf[100];
 gchar *cursor; /* The cursor of writebuf */
 gchar *workbuf_cursor; /* The cursor of workbuf */

 while(i<len) {
   memset(writebuf, 0, 100);
   memset(workbuf, 0, 100);
   strcpy(writebuf, "00000000");
   g_sprintf(workbuf, "%x", i);
   strcpy(&writebuf[strlen(workbuf) >= 8 ? 0 : 8 - strlen(workbuf)], workbuf);
   strcat(writebuf, " |");
   cursor = writebuf + strlen(writebuf);
   memset(workbuf, 0, 100);
   workbuf_cursor = workbuf;
   for(j=0;j<16;j++) {
     if(i+j >= len) {
       strcpy(cursor, "XX ");
       cursor += 3;
       if (j == 7) {
         *(cursor++) = ' ';
       }
       *(workbuf_cursor++) = '*';
       continue;
     }
     guint8 c = ptr[i+j];
     if (c <= 0x0f) {
       strcpy(cursor++, "0");
       g_sprintf(cursor, "%x ", c);
       cursor += 2;
     } else {
       g_sprintf(cursor, "%x ", c);
       cursor += 3;
     }
     if (j == 7) {
       strcpy(cursor++, " ");
     }
     if( j % 2 == 1 ) {
       gunichar unic = ptr[i+j-1] | c<<8;
       if (g_unichar_validate(unic) && g_unichar_isjapanese(unic)) {
         workbuf_cursor += g_unichar_to_utf8(unic, --workbuf_cursor);
         continue;
       } else if (unic == 0x0d) {
         strcpy(workbuf_cursor, "\\n");
         workbuf_cursor += 2;
         continue;
       }
     }
     *(workbuf_cursor++) = isgraph(c) ? c : '.';
   }
   g_print("%s| %s\n", writebuf, workbuf);
   i += 16;
 }
}

void
gppt_mem_dump (guint8 const *ptr, size_t len)
{
 int i = 0;
 int j = 0;
 gchar writebuf[100];
 gchar workbuf[100];
 gchar *cursor;

 while(i<len) {
   memset(writebuf, 0, 100);
   memset(workbuf, 0, 100);
   strcpy(writebuf, "00000000");
   g_sprintf(workbuf, "%x", i);
   strcpy(&writebuf[strlen(workbuf) >= 8 ? 0 : 8 - strlen(workbuf)], workbuf);
   strcat(writebuf, " |");
   cursor = writebuf + strlen(writebuf);
   memset(workbuf, 0, 100);
   for(j=0;j<16;j++) {
     if(i+j >= len) {
       strcpy(cursor, "XX ");
       cursor += 3;
       if (j == 7) {
         *(cursor++) = ' ';
       }
       workbuf[j] = '*';
       continue;
     }
     guint8 c = ptr[i+j];
     if (c <= 0x0f) {
       strcpy(cursor++, "0");
       g_sprintf(cursor, "%x ", c);
       cursor += 2;
     } else {
       g_sprintf(cursor, "%x ", c);
       cursor += 3;
     }
     if (j == 7) {
       strcpy(cursor++, " ");
     }
     workbuf[j] = isgraph(c) ? c : '.';
   }
   g_print("%s| %s\n", writebuf, workbuf);
   i += 16;
 }
}

/*
 * Compare binaries
 * Shows ptr_b dump
 *
 */
void
gppt_mem_compare_dump (guint8 const *ptr_a, guint8 const *ptr_b, size_t len_a, size_t len_b)
{
  guint i=0, j=0;

  size_t min_len = MIN(len_a, len_b);

  while(i<len_b) {
    g_print("%8X |", i);
    for(j=0;j<16;j++) {
      if (len_b <= i+j) {
        g_print("XX ");
      } else if (len_b > min_len) {
	g_print("%sXX%s ", eseq_cap, eseq_end);
      } else if (ptr_a[i+j] == ptr_b[i+j]) {
        g_print("%2X ", ptr_b[i+j]);
      } else {
        g_printf("%s%2X%s", eseq_cap, ptr_b[i+j], eseq_end);
        g_print(" ");
      }
    }
    g_print("|\n");
    i += 16;
  }
}
