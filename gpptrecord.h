#ifndef _GPPT_RECORD_H
#define _GPPT_RECORD_H

#include <glib.h>

const guchar* gppt_get_record_string(gint recid);

#endif /* _GPPT_RECORD_H */
