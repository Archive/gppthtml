#!/usr/bin/perl
#
# recmaster.pl
# Copyright (C) 2003  Yukihiro Nakai <ynakai@redhat.com>
#
# Convert recmaster.txt into a comprehensive header
#
open(RECHEAD, "> gpptrecid.h") || die "Can't open\n";
print RECHEAD<<EOF;
#ifndef _GPPT_RECID_H
#define _GPPT_RECID_H

#include <glib.h>

EOF

open(RECMAS, "recmaster.txt") || die "Can't open\n";
while(<RECMAS>) {
  next if(/^#/);
  ($name, $id) = split;
  $recdic{$id} = $name;
  $name =~ tr/[a-z]/[A-Z]/;
  print RECHEAD sprintf("#define GPPT_RECID_%- 35s %5d\n", $name, $id);
}
close(RECMAS);

print RECHEAD<<EOF;

typedef struct _RecordStr RecordStr;
struct _RecordStr
{
  gint id;
  guchar* str;
};

static RecordStr recordstr[] = 
{

EOF
foreach $i (sort keys %recdic) {
  $str = $name = $recdic{$i};
  $name =~ tr/[a-z]/[A-Z]/;
  print RECHEAD "  {$i, \"$str\" },\n";
}
print RECHEAD<<EOF;
  { -1, NULL }
};

#endif /* _GPPT_RECIT_H */
EOF
close(RECHEAD);
